// Importing the required modules
import { WebSocketServer } from 'ws';
 
// Creating a new websocket server
const wss = new WebSocketServer({ port: 8081 })

import { v4 } from 'uuid';
import { Room } from './models/Room.js';

let intialRoom = new Room('Salle 1')
let rooms = {};
rooms[intialRoom.id] = intialRoom;

let commands = {
    'joinroom': joinRoom,
    'createroom': createRoom,
    'shoot': shoot,
    'placeboat': placeBoat
}

// Creating connection using websocket
wss.on("connection", ws => {

    ws.id = v4();
    ws.send(JSON.stringify({...getPlayerState(ws), message: null}));

    ws.on("message", data => {

        let parsed = JSON.parse(data.toString());
        console.log(parsed)

        let message = null;
        if(commands[parsed.command] != null) {
            message = commands[parsed.command](ws, parsed);
        }

        wss.clients.forEach((client) => {
            client.send(JSON.stringify({...getPlayerState(client), message: client == ws ? message : null}));
        })

    });

    ws.on("close", () => {
    });
    
    ws.onerror = function () {
        console.log("Some Error occurred")
    }
});


function getPlayerState(ws) {

    for(let r of Object.values(rooms)) {
        if(r.isPlayerInGame(ws.id)) {
            return {rooms: null, room: {...r.setDisplayed(ws.id), playerIndex: r.getPlayerIndex(ws.id)}}
        }
    }

    return {rooms: Object.values(rooms), room: null}
}

function getPlayerRoom(ws) {

    for(let r of Object.values(rooms)) {
        if(r.isPlayerInGame(ws.id)) {
            return r
        }
    }
}

function joinRoom(ws, parsed) {
    console.log('joining room')
    if(parsed.id == null || parsed.id?.trim() == '' || rooms[parsed.id] == null) {
        console.log('invalid room id')
        return;
    }
    rooms[parsed.id].addPlayer(ws.id)
}

function createRoom(ws, parsed) {
    console.log('creating room')
    if(parsed.name == null || parsed.name?.trim() == '') {
        console.log('invalid room name')
        return;
    }

    let r = new Room(parsed.name);
    rooms[r.id] = r;

}

function shoot(ws, parsed) {
    console.log('shooting')
    let room = getPlayerRoom(ws);
    if(!room.canPlay(ws.id)) {
        return "Ce n'est pas ton tour !";
    }
    if(!room.getGame().isShootable(parsed.x, parsed.y)) {
        return "Cette case à déjà été jouée!";
    }
    room.getGame().shoot(parsed.x, parsed.y)
    room.getGame().changeTurn()
    console.log(room.getGame().turn)
}

function placeBoat(ws, parsed) {
    console.log('placing boat')
    if(parsed.name == null || parsed.name?.trim() == '') {
        console.log('invalid room name')
        return;
    }

    let r = new Room(parsed.name);
    rooms[r.id] = r;

}

