export class Boat {
    
    nom
    taille
    #direction = 1
    #positions = [];
    playerBoatsPositions;
    couled = false;

    constructor(nom, taille) {
        this.nom = nom;
        this.taille = taille;
    }

    calculerPositions() {
        for(let i = 1; i < this.taille; i++) {
            this.#positions.push({
                x: this.#positions[i-1].x + (1 * !this.#direction),
                y: this.#positions[i-1].y + (1 * this.#direction),
                touched: false,
                bout: i == this.taille - 1
            })
        }
    }

    placer(x, y, dir) {
        this.#direction = dir
        this.#positions.push({
            x,
            y,
            touched: false,
            bout: true,
        });
        this.calculerPositions();
    }

    areAllPosBetweenBounds(xbound, ybound) {
        for(let pos of this.#positions) {
            if(pos.x < 0 || pos.x >= xbound || pos.y < 0 || pos.y >= ybound) {
                return false;
            }
        }
        return true;
    }

    isTouched(x, y) {
        for(let p of this.#positions) {
            if(x == p.x && y == p.y) {
                return true;
            }
        }
        return false;
    }

    setTouched(x, y) {
        for(let p of this.#positions) {
            if(x == p.x && y == p.y) {
                p.touched = true;
            }
        }

        this.couled = this.#positions.every(x => x.touched)

    }

    setDisplayed(bool) {
        this.playerBoatsPositions = bool ? this.#positions : undefined;
    }

}