import { Boat } from "./Boat.js";
import { Cell } from "./Cell.js";

export class Game {

    boardColumns = 9;
    boardRows = 9;
    boards = {0: [], 1: []};
    boats = {0: [], 1: []};
    bateauxDispo = {
        0: [
            new Boat('Porte-avion', 5),
            new Boat('Croiseur', 4),
            new Boat('Contre-torpilleur', 3),
            new Boat('Contre-torpilleur', 3),
            new Boat('Torpilleur', 2),
        ],
        1: [
            new Boat('Porte-avion', 5),
            new Boat('Croiseur', 4),
            new Boat('Contre-torpilleur', 3),
            new Boat('Contre-torpilleur', 3),
            new Boat('Torpilleur', 2),
    ]}

    turn = 0;
    gameState = GameState.PLACE_BOATS

    constructor() {
        this.initializeBoard();
        this.placeBoatsRandom(0)
        this.placeBoatsRandom(1)
    }


    initializeBoard() {
        this.boards[0] = new Array(this.boardRows)
        this.boards[1] = new Array(this.boardRows)
        for(let i = 0; i < this.boardRows; i++) {
            this.boards[0][i] = new Array(this.boardColumns)
            this.boards[1][i] = new Array(this.boardColumns)
            for(let j = 0; j < this.boardColumns; j++) {
                this.boards[0][i][j] = new Cell(i, j);
                this.boards[1][i][j] = new Cell(i, j);
            }
        }
    }


    getBoatsForPlayer(player) {
        return this.boats[player];
    }

    saveBoatForPlayer(player, bateau) {
        if(bateau.dir != 0 && bateau.dir != 1) {
            return 'Direction invalide';
        }
        let b = this.bateauxDispo[player][bateau.id];
        b.placer(bateau.x, bateau.y, bateau.dir);
        if(!b.areAllPosBetweenBounds(this.boardRows, this.boardColumns)) {
            return 'Position invalide !';
        }
        this.boats[player].push(this.bateauxDispo[player].splice(bateau.id, 1)[0]);
        if(this.boats[0].length == 5 && this.boats[1].length == 5) {
            this.gameState = GameState.PLAY;
        }
    }

    changeTurn() {
        this.turn = (this.turn + 1) % 2;
    }

    setDisplayed(index) {
        for(let i = 0; i < this.boardRows; i++) {
            for(let j = 0; j < this.boardColumns; j++) {
                this.boards[(index+1)%2][i][j].boat = undefined;
            }
        }
        for(let b of this.boats[index]) {
            b.setDisplayed(true);
            for(let pos of b.playerBoatsPositions) {
                this.boards[index][pos.x][pos.y].boat = true;
            }
        }
        for(let b of this.boats[(index+1)%2]) {
            b.setDisplayed(false);
        }
    }

    isHisTurn(player) {
        return player != this.turn;
    }

    shoot(x, y) {
        for(let b of this.boats[this.turn]) {
            if(b.isTouched(x, y)) {
                b.setTouched(x, y);
                this.boards[this.turn][x][y].state = 2
                return;
            }
        }
        this.boards[this.turn][x][y].state = 1
    }

    placeBoatsRandom(player) {
        for(let i = this.bateauxDispo[player].length - 1; i >= 0; i--) {
            let b = this.bateauxDispo[player][i];
            let rDir = Math.round(Math.random());
            let rX = Math.floor(Math.random() * this.boardRows);
            let rY = Math.floor(Math.random() * this.boardColumns);
            let cells = []
            for(let j = 0; j < b.taille; j++) {
                let nextX = rX + j * !rDir;
                let nextY = rY + j * rDir;
                if(nextX < 0 || nextX >= this.boardRows || nextY < 0 || nextY >= this.boardColumns || !this.boards[player][nextX][nextY].isPlaceable()) {
                    continue;
                }
                cells.push(this.boards[player][nextX][nextY]);
            }
            if(cells.length != b.taille) {
                i++;
                continue;
            }
            this.saveBoatForPlayer(player, {x: rX, y: rY, dir: rDir, id: i});
            cells.forEach(x => x.setPlaceable(false))
        }
    }

    isShootable(x, y) {
        return this.boards[this.turn][x][y].state === 0
    }

}


const GameState = {
	PLACE_BOATS: 0,
	PLAY: 1,
	FINISHED: 2,
}