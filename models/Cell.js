export class Cell {

    state = 0;
    x;
    y;
    #placeable = true;
    boat;

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }


    isPlaceable() {
        return this.#placeable;
    }

    setPlaceable(p) {
        this.#placeable = p
    }

}