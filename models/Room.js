import { Game } from "./Game.js"
import { v4 } from 'uuid';

export class Room {

    maxPlayers = 2
    players = []
    game = new Game();
    roomName
    id = v4();

    constructor(name) {
        this.roomName = name
    }

    isFull() {
        return this.maxPlayers == this.players.length;
    }

    addPlayer(playerId) {
        if(this.isFull() || this.players.includes(playerId)) {
            return;
        }
        this.players.push(playerId);
    }

    getGame() {
        return this.game;
    }

    isPlayerInGame(playerId) {
        return this.players.includes(playerId);
    }

    getPlayerIndex(playerId) {
        return this.players.indexOf(playerId)
    }

    canPlay(playerId) {
        return this.game.isHisTurn(this.getPlayerIndex(playerId));
    }

    setDisplayed(playerId) {
        let index = this.players.indexOf(playerId);
        this.game.setDisplayed(index)
        return this;
    }

}